-- LUALOCALS < ---------------------------------------------------------
local math, tonumber
    = math, tonumber
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local stamp = tonumber("$Format:%at$")
if not stamp then return end
stamp = math_floor((stamp - 1540612800) / 60)
stamp = ("00000000" .. stamp):sub(-8)

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

return {
	user = "Warr1024",
	pkg = "nc_beacon",
	version = stamp .. "-$Format:%h$",
	type = "mod",
	dev_state = "ACTIVELY_DEVELOPED",
	title = "NodeCore Beacons",
	short_description = "Long-distance navigation beacons for NodeCore",
	tags = {"building", "transport", "world_tools", "technology", "jam_weekly_2021"},
	license = "MIT",
	media_license = "MIT",
	repo = "https://gitlab.com/sztest/nc_beacon",
	issue_tracker = "https://discord.gg/NNYeF6f",
	long_description = readtext('README.md'),
	screenshots = {readbinary('.cdb-screenshot.webp')}
}

-- luacheck: pop
