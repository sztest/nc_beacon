This mods adds long-distance navigation beacons to NodeCore.

See the screenshot included in the package for the recipe: amalgamation beneath an open sky, surrounded by a solid ring of annealed lode, with flux flows on the corners, and inward-pointing Odo charcoal glyphs on the edges.  Pummel the top of the central amalgamation to activate it.

Beacons have an unlimited range for navigation.  Learning how to use them to navigate is left for the player to discover...