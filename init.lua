-- LUALOCALS < ---------------------------------------------------------
local ipairs, math, minetest, nodecore, pairs, string, vector
    = ipairs, math, minetest, nodecore, pairs, string, vector
local math_ceil, math_log, math_random, string_gsub
    = math.ceil, math.log, math.random, string.gsub
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local beaconame = modname .. ":beacon"
local beacondesc = "Beacon"

local particle_range = 80
local particle_max_beacons = 16

local amaldef = minetest.registered_nodes["nc_igneous:amalgam"]
if not amaldef then return end

local recipenodes = {
	{match = "nc_igneous:amalgam"},
	{x = -1, z = -1, match = "nc_lode:block_annealed"},
	{x = -1, match = "nc_lode:block_annealed"},
	{x = -1, z = 1, match = "nc_lode:block_annealed"},
	{x = 1, z = -1, match = "nc_lode:block_annealed"},
	{x = 1, match = "nc_lode:block_annealed"},
	{x = 1, z = 1, match = "nc_lode:block_annealed"},
	{z = -1, match = "nc_lode:block_annealed"},
	{z = 1, match = "nc_lode:block_annealed"},
	{x = -1, y = 1, z = -1, match = "nc_lux:flux_flowing"},
	{x = -1, y = 1, z = 1, match = "nc_lux:flux_flowing"},
	{x = 1, y = 1, z = -1, match = "nc_lux:flux_flowing"},
	{x = 1, y = 1, z = 1, match = "nc_lux:flux_flowing"},
	{x = -1, y = 1, match = {
			name = "nc_writing:glyph2",
			param2 = 3
	}},
	{x = 1, y = 1, match = {
			name = "nc_writing:glyph2",
			param2 = 1
	}},
	{y = 1, z = -1, match = {
			name = "nc_writing:glyph2",
			param2 = 2
	}},
	{y = 1, z = 1, match = {
			name = "nc_writing:glyph2",
			param2 = 0
	}},
	{y = 1, match = "air", replace = beaconame},
}

------------------------------------------------------------------------
-- Beacon Database

local allpos, setpos, rmpos
do
	local modstore = minetest.get_mod_storage()
	local db = modstore:get_string("data")
	db = db and minetest.deserialize(db) or {}
	allpos = function() return pairs(db) end
	local function savedb()
		return modstore:set_string("data", minetest.serialize(db))
	end
	setpos = function(pos)
		local key = minetest.pos_to_string(pos, 0)
		if db[key] then return end
		db[key] = pos
		return savedb()
	end
	rmpos = function(pos)
		local key = minetest.pos_to_string(pos, 0)
		if not db[key] then return end
		db[key] = nil
		return savedb()
	end
end

do
	local queue = {}
	minetest.register_globalstep(function()
			if #queue < 1 then
				for _, p in allpos() do
					queue[#queue + 1] = p
				end
				if #queue < 1 then return end
			end
			local pos = queue[#queue]
			queue[#queue] = nil
			local node = minetest.get_node_or_nil(pos)
			if node and (node.name ~= beaconame) then
				return rmpos(pos)
			end
		end)
end

------------------------------------------------------------------------
-- Beacon Check

local function opensky(pos)
	return minetest.get_node_light({
			x = pos.x,
			y = pos.y + 1,
			z = pos.z
		}, 0.5) == nodecore.light_sun
end

local function beaconfail(pos)
	rmpos(pos)
	return minetest.remove_node(pos)
end

local function beaconcheck(pos)
	if minetest.get_node(pos).name ~= beaconame
	then return rmpos(pos) end

	if not opensky(pos) then return beaconfail(pos) end

	for _, chk in pairs(recipenodes) do
		local chkp = {
			x = pos.x + (chk.x or 0),
			y = pos.y + (chk.y or 0) - 1,
			z = pos.z + (chk.z or 0)
		}
		if (not vector.equals(chkp, pos))
		and chk.match
		and (not nodecore.match(chkp, chk.match))
		then return beaconfail(pos) end
	end

	setpos(pos)
end

------------------------------------------------------------------------
-- Node Registration and Craft

nodecore.register_node(beaconame, {
		description = beacondesc,
		drawtype = "airlike",
		pointable = false,
		walkable = false,
		climbable = false,
		buildable_to = true,
		floodable = true,
		air_equivalent = true,
		paramtype = "light",
		sunlight_propagates = true,
		on_node_touchthru = function(pos, _, under, player)
			local raw = nodecore.touchtip_node(under, nil, player)
			if raw and under.y == pos.y - 1 then
				return raw .. "\n" .. beacondesc
			end
			return raw
		end
	})

nodecore.register_craft({
		label = "create beacon",
		action = "pummel",
		toolgroups = {thumpy = 1},
		normal = {y = 1},
		indexkeys = {"nc_igneous:amalgam"},
		check = opensky,
		nodes = recipenodes,
		after = function(pos)
			return beaconcheck({x = pos.x, y = pos.y + 1, z = pos.z})
		end
	})

------------------------------------------------------------------------
-- Recheck ABM

nodecore.register_abm({
		label = modname .. " check",
		nodenames = {beaconame},
		interval = 2,
		chance = 1,
		action = beaconcheck
	})

------------------------------------------------------------------------
-- Skybox Mod

do
	local beaconmod = ""

	local function esc(t) return string_gsub(string_gsub(t, "%^", "\\^"), ":", "\\:") end
	local txrspot = esc("^[resize:256x256")
	local txrpattern = string_gsub(txrspot, "%W", "%%%1")

	local posscale = 128 / math_log(32768)
	nodecore.interval(1, function()
			local spawn = minetest.setting_get_pos("static_spawnpoint")
			or {x = 0, y = 0, z = 0}

			beaconmod = ""
			for _, bpos in allpos() do
				local pos = vector.subtract(bpos, spawn)
				pos.y = 0
				local dist = vector.length(pos)
				local log = math_log(dist + 1) * posscale
				pos = vector.multiply(pos, log / dist)
				local px = math_ceil(pos.z)
				local py = math_ceil(-pos.x)
				beaconmod = beaconmod .. ":" .. (123 + px) .. "," .. (123 + py)
				.. "=" .. esc("nc_beacon_star.png^[resize:11x11")
			end
		end)

	nodecore.register_playerstep({
			label = "beacon skybox",
			priority = -10,
			action = function(_, data)
				if beaconmod ~= "" then
					data.sky.textures[1] = string_gsub(
						data.sky.textures[1],
						txrpattern,
						txrspot .. beaconmod)
				end
			end
		})
end

------------------------------------------------------------------------
-- Particles

local function intersect(mina, maxa, minb, maxb)
	if mina.x > maxb.x or minb.x > maxa.x
	or mina.y > maxb.y or minb.y > maxa.y
	or mina.z > maxb.z or minb.z > maxa.z
	then return end
	return {
		x = mina.x > minb.x and mina.x or minb.x,
		y = mina.y > minb.y and mina.y or minb.y,
		z = mina.z > minb.z and mina.z or minb.z,
		}, {
		x = maxa.x < maxb.x and maxa.x or maxb.x,
		y = maxa.y < maxb.y and maxa.y or maxb.y,
		z = maxa.z < maxb.z and maxa.z or maxb.z,
	}
end

local function particles_clipped(def)
	local rawmin = def.minpos
	local rawmax = def.maxpos
	for _, player in ipairs(minetest.get_connected_players()) do
		local pos = vector.add(player:get_pos(), player:get_velocity())
		pos.y = pos.y + player:get_properties().eye_height
		local minp, maxp = intersect(rawmin, rawmax,
			{
				x = pos.x - particle_range,
				y = pos.y - particle_range,
				z = pos.z - particle_range
			},
			{
				x = pos.x + particle_range,
				y = pos.y + particle_range,
				z = pos.z + particle_range
			})
		if minp then
			local newdef = {}
			for k, v in pairs(def) do newdef[k] = v end
			newdef.amount = newdef.amount * (maxp.x - minp.x)
			* (maxp.y - minp.y) * (maxp.z - minp.z)
			newdef.minpos = minp
			newdef.maxpos = maxp
			newdef.playername = player:get_player_name()
			minetest.add_particlespawner(newdef)
		end
	end
end

local function beaconparticles()
	local t = {}
	for _, v in allpos() do t[#t + 1] = v end
	for i = #t, 2, -1 do
		local j = math_random(1, i)
		local x = t[i]
		t[i] = t[j]
		t[j] = x
	end
	local limit = #t
	if limit > particle_max_beacons then limit = particle_max_beacons end
	for i = 1, limit do
		local pos = t[i]
		particles_clipped({
				amount = 2,
				time = 2,
				minpos = vector.offset(pos, -0.5, -0.5, -0.5),
				maxpos = vector.offset(pos, 0.5, 10000 - pos.y, 0.5),
				minvel = {x = 0, y = 0, z = 0},
				maxvel = {x = 0, y = 5, z = 0},
				minacc = {x = 0, y = 5, z = 0},
				maxacc = {x = 0, y = 5, z = 0},
				minexptime = 1,
				maxexptime = 2,
				minsize = 1,
				maxsize = 2,
				glow = 14,
				node = {name = "nc_terrain:lava_source"}
			})
		particles_clipped({
				amount = 500,
				time = 2,
				minpos = vector.offset(pos, -0.5, -0.5, -0.5),
				maxpos = vector.offset(pos, 0.5, -0.45, 0.5),
				minvel = {x = 0, y = 0, z = 0},
				maxvel = {x = 0, y = 0, z = 0},
				minacc = {x = 0, y = 5, z = 0},
				maxacc = {x = 0, y = 5, z = 0},
				minexptime = 1,
				maxexptime = 2,
				minsize = 1,
				maxsize = 2,
				glow = 14,
				node = {name = "nc_terrain:lava_source"}
			})
	end
	minetest.after(2, beaconparticles)
end
minetest.after(0, beaconparticles)
